package bugZapper;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.Timer;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;

import framework.Board;
import framework.Player;

import bugZapper.boardElements.Bug;
import bugZapper.boardElements.Tower;
import bugZapper.boardElements.ZapperBoard;
import bugZapper.boardElements.ZapperTile;
import org.eclipse.swt.events.MouseTrackAdapter;

public class BugZapper extends ApplicationWindow {

	private static ZapperBoard gameBoard;
	
	private static MessageBox messageBox;
	
	private static Composite container;
	private static Composite gameComp;
	
	private static Timer attackTimer;
	private static Timer moveTimer;
	
	private static ArrayList<Tower> towerList;
	
	private static int numEnemies = 40;
	private static int numLives = 40;
	private static int moneyBalance = 5000;
	
	private static boolean doUpgrade = false;
	private static boolean placingTower;
	
	/**
	 * Create the application window.
	 */
	public BugZapper() {
		super(null);
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
	}
	
	public void buildBoard() {
		
		//w/h of each cell
		GridData gd= new GridData();
		
		gd.widthHint = ((Display.getCurrent().getBounds().width - 40) / 2) / gameBoard.getWidth();
		gd.heightHint = (Display.getCurrent().getBounds().height - 184) / gameBoard.getHeight();
		
		boolean firstIteration = true;
		for(int i=0; i<gameBoard.getLength(); i++){
			for(int j = 0; j< gameBoard.getHeight(); j++){
				final int row = i;
				final int column = j;
				
				final Label label = new Label(gameComp, SWT.NONE);

				label.setLayoutData(gd);
				label.addMouseTrackListener(new MouseTrackAdapter() {
					@Override
					public void mouseEnter(MouseEvent e) {
						if (placingTower) {
							if (!gameBoard.getTile(row, column).checkIsPath()) {
								label.setBackgroundImage(SWTResourceManager.getImage("src\\main\\resources\\tower.jpg"));
							}
							
						}
					}
					@Override
					public void mouseExit(MouseEvent e) {
						
						if (placingTower && !gameBoard.getTile(row, column).checkOccupied((ArrayList<Player>)((ArrayList<?>)towerList))) {
							if (!gameBoard.getTile(row, column).checkIsPath()) {
								label.setBackgroundImage(null);
							}
						}
						
					}
				});
				label.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseUp(MouseEvent arg0) {
						if (!gameBoard.getTile(row, column).checkIsPath() && 
										!gameBoard.getTile(row, column).checkOccupied((ArrayList<Player>)((ArrayList<?>)towerList))) { 
							// Need to decide how much towers cost. 
							moneyBalance -= 500;
							placingTower = false;
							
							Tower tower = new Tower("Pew Pew", 5, 50, gameBoard, gameBoard.getTile(row, column));
							tower.setRow(row);
							tower.setCol(column);
							
							towerList.add(tower);
							
						} else {
							messageBox.setText("Occupied!");
							messageBox.setMessage("You cannot place a tower here.");
							messageBox.open();
						}
					}
				});
				
				//set up a special tile on each location on the board 
				//that the player cannot build on, and that the npcs will 
				//use as a path- the path is a '1' in the board.txt file
				if(gameBoard.getTile(i, j).checkIsPath()){
					
					
					//set the gameBoard location to a tile that cannot be built on
					ZapperTile pathTile = new ZapperTile(i,j,gameBoard.getTile(i, j).getCellValue());
					gameBoard.setTile(i, j, pathTile);
					if (gameBoard.getTile(i, j).checkIsPath() && firstIteration) {
						
						gameBoard.setStartingTile(i, j);
						firstIteration = false;
					}
					
					if (gameBoard.getTile(i,j).getCellValue() == 6) {
						
						gameBoard.setFinishTile(i,j);
					}

					label.setBackgroundImage(SWTResourceManager.getImage("src\\main\\resources\\lightning_bolt.jpg"));
				}
			}
		}
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		
		container = new Composite(parent, SWT.NONE);
		container.setLayout(new FormLayout());
		Composite hudComp = new Composite(container, SWT.NONE);
		FormData fd_hudComp = new FormData();

		fd_hudComp.left = new FormAttachment(0);
		fd_hudComp.right = new FormAttachment(100);
		fd_hudComp.bottom = new FormAttachment(100);
		hudComp.setLayoutData(fd_hudComp);
		
		gameComp = new Composite(container, SWT.NONE);
		//gameComp.setLayout(new GridLayout(gameBoard.getWidth(), false));
		FormData fd_gameComp = new FormData();
		fd_gameComp.top = new FormAttachment(0);
		fd_gameComp.left = new FormAttachment(0);
		fd_gameComp.bottom = new FormAttachment(0, 374);
		fd_gameComp.right = new FormAttachment(0, 454);
		gameComp.setLayoutData(fd_gameComp);
		GridLayout gl_gameComp = new GridLayout(gameBoard.getWidth(), true);
		
		//temporary buffers for grid alignment
		gl_gameComp.horizontalSpacing = 0;
		gl_gameComp.verticalSpacing = 0;
		
		gameComp.setLayout(gl_gameComp);
		gameComp.setBackgroundImage(SWTResourceManager.getImage("src\\main\\resources\\circuit_background.jpg"));

		buildBoard();
		
		fd_gameComp.bottom = new FormAttachment(hudComp, -6);
		fd_gameComp.right = new FormAttachment(hudComp, 0, SWT.RIGHT);
		
		Label statLabel = new Label(hudComp, SWT.NONE);
		statLabel.setBounds(0, 0, 140, 64);
		statLabel.setText("Stats:");
		
		Button buyTowerButton = new Button(hudComp, SWT.NONE);
		buyTowerButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				if (moneyBalance >= 500) {
					placingTower = true;
				} else {
					messageBox.setText("Insufficient Funds");
					messageBox.setMessage("Not enough money. Kill more bugs!");
				}

			}
		});
		buyTowerButton.setBounds(158, 29, 75, 25);
		buyTowerButton.setText("Buy Tower");
		
		Button upgradeButton = new Button(hudComp, SWT.NONE);
		upgradeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				new UpgradePrompt(new Shell(), moneyBalance, new Player("")).open();
			}
		});
		upgradeButton.setBounds(255, 29, 98, 25);
		upgradeButton.setText("Upgrade Towers");
		
		Button sellButton = new Button(hudComp, SWT.NONE);
		sellButton.setBounds(377, 29, 75, 25);
		sellButton.setText("Sell Towers");
		
		Button startStopButton = new Button(hudComp, SWT.NONE);
		startStopButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(attackTimer.isRunning()){
					System.out.println("Stopped");
					moveTimer.stop();
					attackTimer.stop();
					
				}else{
					System.out.println("Started");
					moveTimer.start();
					attackTimer.start();
					
				}
			}
		});
		startStopButton.setBounds(470, 29, 104, 25);
		startStopButton.setText("Start/Pause Round");
		
		Button restartButton = new Button(hudComp, SWT.NONE);
		restartButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				attackTimer.stop();
				moveTimer.stop();
				
				messageBox = new MessageBox(new Shell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				messageBox.setText("Restart?");
				messageBox.setMessage("Are you sure you want to restart the current round?");
				if (messageBox.open() == SWT.YES) {
					
					towerList = new ArrayList<Tower>();
					gameBoard.setPlayerList(new ArrayList<Player>());
					
					attackTimer.restart();
					attackTimer.stop();
					moveTimer.restart();
					moveTimer.stop();
					
					Control[] labels = gameComp.getChildren();
					for (int i = 0; i < labels.length; i++) {
						labels[i].dispose();
					}
					
					buildBoard();
					Control [] newLabels = gameComp.getChildren();
					for (int i = 0; i < labels.length; i++) {
						newLabels[i].moveAbove(labels[i]);
					}
				}

			}
		});
		restartButton.setBounds(592, 29, 86, 25);
		restartButton.setText("Restart Round");


		return container;
	}
	
	private static class AttackEvent implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			if (!towerList.isEmpty()) {
					
				Iterator<Tower> towerItr = towerList.listIterator();
				while (towerItr.hasNext()) {
					Tower tower = towerItr.next();
					
					if (tower.getCurrentTarget() != null && tower.enemyReachable()) {
						// &&  && tower.enemyReachable()
						tower.attackTarget();
						System.out.println("Attacking");
						if (tower.getCurrentTarget() != null) 
							System.out.println(tower.getCurrentTarget().getProperties().get("Health"));
							
					} else {
						System.out.println("Finding Enemy");
						tower.findEnemy();
						tower.attackTarget();
						if (tower.getCurrentTarget() != null) 
							System.out.println(tower.getCurrentTarget().getProperties().get("Health"));
						else 
							System.out.println("target null");
					}
				}
			}
		}
	}
	
	private static class MoveEvent implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if (numEnemies > 0) {

				gameBoard.getPlayerList().add(new Bug("Stack Overflow", gameBoard.getStartingTile(), 500));
				numEnemies--;
				
			}
			
			ArrayList<Player> enemyList = gameBoard.getPlayerList();
			
			for (int i = 0; i < enemyList.size(); i++) {
				
				gameBoard.setCurrentPlayer(enemyList.get(i));
				gameBoard.setRollNum(1);
				gameBoard.movement();
				System.out.println("Bug: " + i + " row: " + gameBoard.getCurrentPlayer().getCurrentTile().getRow() + " column: " + gameBoard.getCurrentPlayer().getCurrentTile().getCol());
				if (gameBoard.getWinState() == 1) {
					numLives--;
					gameBoard.setWinState(0);
					System.out.println("Life Lost");
					if (numLives < 1) {
						moveTimer.stop();
						// Notify User that they suck. 
					}
					enemyList.remove(enemyList.get(i));
				}
				// Update GUI pictures
			}
		
		}
	}

	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		return menuManager;
	}

	/**
	 * Create the toolbar manager.
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		ToolBarManager toolBarManager = new ToolBarManager(style);
		return toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}
	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}


	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		
		attackTimer = new Timer(100, new AttackEvent());
		moveTimer = new Timer(200, new MoveEvent());
		
		try {
			gameBoard = new ZapperBoard(8, 8, "board.txt");
		} catch (Exception e1) {
			
			
		}
		
		towerList = new ArrayList<Tower>();
		messageBox = new MessageBox(new Shell(), SWT.ICON_ERROR);
		
		try {
			BugZapper window = new BugZapper();
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		} catch (Exception e) {
			
		}
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Bug Zapper");
		newShell.setBackgroundMode(SWT.INHERIT_DEFAULT);
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(Display.getCurrent().getBounds().width / 2, Display.getCurrent().getBounds().height);
	}
}
