----------BOARD READ ME------------

Framework By: The KandyMen (Group 10)
BitBucket Repository: https://bitbucket.org/kandymen/kandy-dungeon-framework
Contact: StickelM@cwu.edu

-----------------------------------

Altering the Board Path (Board.txt):

The actual path of the board is determined by an external file (board.txt) that
can be altered in order to change the path as the developer desires.The framework 
allows the player to traverse the path of the board by using a number system 
for the direction of travel. The different numbers of the path tell the framework 
which direction the Player is allowed to travel in order to prevent traveling 
in the wrong direction. The starting and finishing tiles are CONSTANT variables 
within Board.java.

-----------------------------------

Path Number System:

0 - Non-traversable board tiles
1 - East
2 - South
3 - North
4 - West