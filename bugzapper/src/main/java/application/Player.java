package application;

/**
 * @author Michael Stickel
 * 
 */
public class Player {

	protected Tile currentTile;
	private String name;
	private int row;
	private int col;

	/**
	 * Constructor for Player class
	 * 
	 * @param name
	 * @param startTile
	 */
	public Player(String name, Tile startTile) {
		this.name = name;
		this.currentTile = startTile;
	}

	/**
	 * Constructor for Player class
	 * 
	 * @param name
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Changes current position of player
	 * 
	 * @param nextTile
	 */
	public void changeTile(Tile nextTile) {
		this.currentTile = nextTile;
	}

	/**
	 * @return currentTile
	 */
	public Tile getCurrentTile() {
		return currentTile;
	}

	/**
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return col
	 */
	public int getCol() {
		return col;
	}
}
