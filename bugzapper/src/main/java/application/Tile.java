package application;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Michael Stickel
 * 
 */
public class Tile {

	private int row;
	private int col;
	private int cellValue;

	/**
	 * Constructor for Tile class
	 * 
	 * @param row
	 * @param col
	 * @param cellValue
	 */
	public Tile(int row, int col, int cellValue) {
		this.row = row;
		this.col = col;
		this.cellValue = cellValue;
	}

	/**
	 * Constructor for Tile class
	 * 
	 * @param row
	 * @param col
	 */
	public Tile(int row, int col) {
		this.row = row;
		this.col = col;
	}

	/**
	 * Constructor for Tile class
	 */
	public Tile() {
	}

	/**
	 * Check if nextTile is occupied
	 * 
	 * @param playerList - list of Players
	 * @return true - if occupied, false - if not occupied
	 */
	public boolean checkOccupied(ArrayList<Player> playerList) {
		boolean isOccupied = false;

		for (int i = 0; i < playerList.size(); i++) {
			if (row == playerList.get(i).getRow()
					&& col == playerList.get(i).getCol()) {
				isOccupied = true;
			}
		}
		return isOccupied;
	}

	/**
	 * Determines if tile will throw an event when landed on.
	 * 
	 * @param chance
	 *            - Chance that event will be thrown
	 * @param numChances
	 *            - Number of Chances that Chance will come up.
	 *            
	 *            | example: chance = 0 numChances = 3					  |
	 *            | There is a 1 in 3 chance that an event will be thrown.|
	 * 
	 * @return true - If event should be thrown false - If event should not be
	 *         thrown
	 */
	public boolean throwEvent(int chance, int numChances) {
		Random rand = new Random();
		int num = rand.nextInt(numChances);

		if (num == chance) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return col
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Determines if tile is a designated path that can be traveled by player
	 * 
	 * @return true - If tile is a path false - If tile is not a path
	 */
	public boolean checkIsPath() {
		if (cellValue != 0)
			return true;
		else
			return false;
	}

	/**
	 * Checks which direction the path is going. Must be set in order for player
	 * to properly traverse the board.
	 * 
	 * @return
	 */
	public String pathDirection() {
		if (cellValue == 1)
			return "East";
		else if (cellValue == 2)
			return "South";
		else if (cellValue == 3)
			return "North";
		else
			return "West";
	}
}
