package application;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

/**
 * @author Michael Stickel
 * 
 */
public class Board {

	private int width;
	private int height;
	private int turn = 0;
	private int rollNum;
	private File file;
	private Tile[][] board;
	private ArrayList<Player> playerList;
	private Player currentPlayer;
	private Player winningPlayer;
	private int winState = 0;
	
	private final Tile STARTING_TILE = board[0][0];
	private final Tile FINISH_TILE = board[7][7];

	/**
	 * Constructor for Board class. Populates board from txt file.
	 * 
	 * @param width
	 * @param height
	 * @throws Exception
	 */
	public Board(int width, int height) throws Exception {
		this.width = width;
		this.height = height;

		playerList = new ArrayList<Player>();

		board = new Tile[height][width];

		file = new File("board.txt");	// Contains Board Path
		Scanner input = new Scanner(file);

		int cellValue;

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				cellValue = input.nextInt();
				board[row][col] = new Tile(row, col, cellValue);
			}
		}
	}

	/**
	 * Displays board of tiles using checkIsPath() method to determine game
	 * path.
	 */
	public void displayBoard() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				System.out.print(board[row][col].checkIsPath());
				System.out.print(" ");
			}
			System.out.println("\n");
		}
	}

	/**
	 * Generates a random dice roll between 1 and 6
	 */
	public void roll() {
		Random rand = new Random();

		rollNum = rand.nextInt(6) + 1;
	}

	/**
	 * Getter method returns index of current player turn within playerlist
	 * 
	 * @return - turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * Sets the currentPlayer depending on turn field
	 * 
	 * @param turn
	 */
	public void setTurn(int turn) {
		this.turn = turn;
		currentPlayer = playerList.get(turn);
	}

	/**
	 * Ends current turn by incrementing turn field. Updates setTurn with new
	 * turn value.
	 */
	public void endTurn() {
		turn++;

		if (turn == playerList.size()) {
			turn = 0;
		}

		setTurn(turn);
	}

	/**
	 * Moves currentPlayer around board depending on number rolled
	 */
	public void movement() {
		int numLeft = rollNum;
		int count = 0;
		int row = currentPlayer.currentTile.getRow();
		int col = currentPlayer.currentTile.getCol();
		Tile nextTile = board[row][col];
		
		final int NORTH_ROW = row - 1, 
				  NORTH_COL = col;
		
		final int EAST_ROW = row, 
				  EAST_COL = col + 1;
		
		final int SOUTH_ROW = row + 1, 
				  SOUTH_COL = col;
		
		final int WEST_ROW = row, 
				  WEST_COL = col - 1;

		while (numLeft != 0) {
			numLeft--; // Decrements value of roll every loop

			switch (count++) {
			case 0: // north (path value = 3)
				if (isOnBoard(NORTH_ROW, NORTH_COL) == true
						&& board[NORTH_ROW][NORTH_COL].checkIsPath() == true
						&& board[NORTH_ROW][NORTH_COL].pathDirection().equals("North")) {
					nextTile = board[NORTH_ROW][NORTH_COL]; 
					count = 0;
					break;
				}
			case 1: // east (path value = 1)
				if (isOnBoard(EAST_ROW, EAST_COL) == true
						&& board[EAST_ROW][EAST_COL].checkIsPath() == true
						&& board[EAST_ROW][EAST_COL].pathDirection().equals("East")) {
					nextTile = board[EAST_ROW][EAST_COL]; 
					count = 0;
					break;
				}
			case 2: // south (path value = 2)
				if (isOnBoard(SOUTH_ROW, SOUTH_COL) == true
						&& board[SOUTH_ROW][SOUTH_COL].checkIsPath() == true
						&& board[SOUTH_ROW][SOUTH_COL].pathDirection().equals("South")) {
					nextTile = board[SOUTH_ROW][SOUTH_COL]; 
					count = 0;
					break;
				}
			case 3: // west (path value = 4)
				if (isOnBoard(WEST_ROW, WEST_COL) == true
						&& board[WEST_ROW][WEST_COL].checkIsPath() == true
						&& board[WEST_ROW][WEST_COL].pathDirection().equals("West")) {
					nextTile = board[WEST_ROW][WEST_COL]; 
					count = 0;
					break;
				}
			default: count = 0;
			}
			row = nextTile.getRow(); // Updates row after move
			col = nextTile.getCol(); // updates col after move

		}
		if (isWinner(nextTile) == true) {  // Checks if next tile causes player to win
			winningPlayer = currentPlayer;
			winState = 1;				   
		}
		currentPlayer.changeTile(nextTile); // Moves player location
	}

	/**
	 * Determines whether next possible move is in bounds
	 * 
	 * @param row
	 *            - row number to check
	 * @param col
	 *            - col number to check
	 * 
	 * @return true - if next move is on board false - if next move is off board
	 */
	public boolean isOnBoard(int row, int col) {
		if (row >= 0 && col >= 0 && row < height && col < width)
			return true;
		else
			return false;
	}

	/**
	 * Checks to see if next move causes win
	 * 
	 * @param nextTile
	 *            - next possible tile to move onto
	 * 
	 * @return true - if next tile is winner false - if next tile is not winner
	 */
	public boolean isWinner(Tile nextTile) {
		if (nextTile.getRow() == FINISH_TILE.getRow() && nextTile.getCol() == FINISH_TILE.getCol())
			return true;
		else
			return false;
	}

	/**
	 * Returns the value of the rolled number
	 * 
	 * @return - rollNum
	 */
	public int getRoll() {
		return rollNum;
	}

	/**
	 * Returns the current tile position.
	 * 
	 * @param row - current row
	 * @param col - current column
	 * @return
	 */
	public Tile getTile(int row, int col) {
		return board[row][col];
	}

	/**
	 * Adds players to Player List by name and starting tile.
	 * 
	 * @param name
	 */
	public void setPlayers(String name) {
		playerList.add(new Player(name, STARTING_TILE));
	}

	/**
	 * @return currentPlayer
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * @return playerList
	 */
	public ArrayList<Player> getPlayerList() {
		return playerList;
	}

	/**
	 * @return length of board
	 */
	public int getBoardLength() {
		return board.length;
	}

	/**
	 * @return return winningPlayer
	 */
	public Player getWinner() {
		return winningPlayer;
	}

	/**
	 * @return WinState
	 */
	public int getWinState() {
		return winState;
	}
}
