package application.events;
import org.springframework.context.ApplicationEvent;

public class BackTrackTwoPlaces extends ApplicationEvent {

	public BackTrackTwoPlaces(Object source){
		super(source);
	}
	
	public String toString(){
		return "Sorry. Go back 2 places.";
	}
}
