package application.events;
import org.springframework.context.ApplicationEvent;

public class MoveForwardOne extends ApplicationEvent {

	public MoveForwardOne(Object source){
		super(source);
	}
	
	public String toString(){
		return "Nice. Move forward one spot.";
	}
}
