package application.events;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class RunEvent {
	public static void main(String[] args) throws InterruptedException {
	      ConfigurableApplicationContext context = 
	      new ClassPathXmlApplicationContext("event.xml");
	      
	      // To start event
	      context.start();
	      
	      CustEventPublisher cvp = 
	    		  (CustEventPublisher) context.getBean("customEventPublisher");
	      cvp.publish1();
	      cvp.publish2();
	      cvp.publish3();
	      cvp.publish4();
	      TheEvent ev = (TheEvent) context.getBean("theEvent");
	       
	      ev.getEvent();
	      
	      // To stop event
	      context.stop();
	
	}
}
