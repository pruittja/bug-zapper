package application;

/**
 * @author Michael Stickel
 * 
 */
public interface Event {

	/**
	 * @return
	 */
	public Event pickEvent();
}
