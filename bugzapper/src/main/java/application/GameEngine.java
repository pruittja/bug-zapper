package application;

/**
 * @author Michael Stickel
 * 
 */
public class GameEngine {

	private int width; // Width of board
	private int height; // Height of Board

	/**
	 * 
	 */
	public GameEngine() {

	}

	/**
	 * Alternate means of running program without GUI primarily for testing
	 * purposes
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception {

		setSize(8, 8);

		Board board = new Board(width, height);

		board.setPlayers("Michael");
		board.setTurn(0); // initial turn must be set.

		board.roll();
		board.movement();
	}

	/**
	 * Sets size of board externally. Must be set before creating board.
	 * 
	 * @param width
	 * @param height
	 */
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public static void main(String[] args) throws Exception {
		new GameEngine().run();

	}
}
