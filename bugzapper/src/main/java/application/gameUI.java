package application;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import swing2swt.layout.FlowLayout;

public class gameUI {
	Board board;

	private class ContentProvider implements IStructuredContentProvider {
		public Object[] getElements(Object inputElement) {
			return new Object[0];
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	protected Shell shlKandyDungeon;
	private Text text;
	private Table boardViewer;
	private Text newPlayerName;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			gameUI window = new gameUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	;

	public void open() throws Exception {
		board = new Board(8, 8);
		board.setPlayers("New Player");
		board.setTurn(0); // initial turn is set.

		Display display = Display.getDefault();
		createContents();
		shlKandyDungeon.open();
		shlKandyDungeon.layout();
		while (!shlKandyDungeon.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlKandyDungeon = new Shell();
		shlKandyDungeon.setSize(588, 459);
		shlKandyDungeon.setText("Kandy Dungeon");
		shlKandyDungeon.setLayout(new FormLayout());
		Color gray = new Color(null, 139, 137, 137);
		Color white = new Color(null, 200, 200, 200);
		Menu menu = new Menu(shlKandyDungeon, SWT.BAR);
		shlKandyDungeon.setMenuBar(menu);

		Label label = new Label(shlKandyDungeon, SWT.SEPARATOR | SWT.VERTICAL);
		FormData fd_label = new FormData();
		fd_label.bottom = new FormAttachment(0, 306);
		fd_label.right = new FormAttachment(0, 372);
		fd_label.top = new FormAttachment(0, 5);
		fd_label.left = new FormAttachment(0, 371);
		label.setLayoutData(fd_label);

		final ListViewer listViewer = new ListViewer(shlKandyDungeon,
				SWT.BORDER | SWT.V_SCROLL);
		List list = listViewer.getList();
		int playerCount = 0;
		//for (playerCount = 0; playerCount < board.getPlayerList().size(); playerCount++) {
			//listViewer.add(board.getPlayerList().get(playerCount).getName());
		//}
		FormData fd_list = new FormData();
		fd_list.bottom = new FormAttachment(0, 144);
		fd_list.right = new FormAttachment(0, 567);
		fd_list.top = new FormAttachment(0, 5);
		fd_list.left = new FormAttachment(0, 378);
		list.setLayoutData(fd_list);
		listViewer.setContentProvider(new ContentProvider());

		Group grpGameplayButtons = new Group(shlKandyDungeon, SWT.NONE);
		FormData fd_grpGameplayButtons = new FormData();
		fd_grpGameplayButtons.bottom = new FormAttachment(label, 0, SWT.BOTTOM);
		fd_grpGameplayButtons.right = new FormAttachment(100, -5);
		fd_grpGameplayButtons.top = new FormAttachment(0, 152);
		fd_grpGameplayButtons.left = new FormAttachment(0, 378);
		grpGameplayButtons.setLayoutData(fd_grpGameplayButtons);
		grpGameplayButtons.setText("Gameplay Buttons");
		grpGameplayButtons.setLayout(new FillLayout(SWT.VERTICAL));

		Button btnRoll = new Button(grpGameplayButtons, SWT.FLAT | SWT.CENTER);
		btnRoll.setGrayed(true);
		btnRoll.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				board.roll();

				text.setText(board.getCurrentPlayer().getName() + " rolled a "
						+ board.getRoll());
				board.movement();

				text.setText(board.getCurrentPlayer().getName()
						+ " is now on Tile "
						+ board.getCurrentPlayer().currentTile.getCol() + ", "
						+ board.getCurrentPlayer().currentTile.getRow());

				if (board.getWinState() == 1) {
					text.setText(board.getCurrentPlayer().getName()
							+ " has escaped the Kandy Dungeon!\n"
							+ board.getCurrentPlayer().getName()
							+ " is the winner!!");
				}

			}
		});
		btnRoll.setText("Roll");

		Button btnEndTurn = new Button(grpGameplayButtons, SWT.FLAT
				| SWT.CENTER);
		btnEndTurn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				board.endTurn();
				text.setText("It is now " + board.getCurrentPlayer().getName()
						+ "'s turn!");
			}
		});
		btnEndTurn.setText("End Turn");

		Label label_1 = new Label(shlKandyDungeon, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		FormData fd_label_1 = new FormData();
		fd_label_1.right = new FormAttachment(0, 373);
		fd_label_1.top = new FormAttachment(0, 312);
		fd_label_1.left = new FormAttachment(0, 5);
		label_1.setLayoutData(fd_label_1);

		text = new Text(shlKandyDungeon, SWT.BORDER | SWT.V_SCROLL);
		FormData fd_text = new FormData();
		fd_text.bottom = new FormAttachment(0, 391);
		fd_text.right = new FormAttachment(0, 372);
		fd_text.top = new FormAttachment(0, 312);
		fd_text.left = new FormAttachment(0);
		text.setLayoutData(fd_text);
		text.setEditable(false);
		text.setText("Welcome to the Kandy Dungeon!\n Add a new player! ");

		boardViewer = new Table(shlKandyDungeon, SWT.FULL_SELECTION
				| SWT.HIDE_SELECTION);
		FormData fd_boardViewer = new FormData();
		fd_boardViewer.bottom = new FormAttachment(label, 0, SWT.BOTTOM);
		fd_boardViewer.right = new FormAttachment(label, -6);
		fd_boardViewer.top = new FormAttachment(label, 0, SWT.TOP);
		fd_boardViewer.left = new FormAttachment(label_1, 0, SWT.LEFT);
		boardViewer.setLayoutData(fd_boardViewer);
		boardViewer.setHeaderVisible(false);
		boardViewer.setLinesVisible(true);

		int count = 8;
		for (int i = 0; i < board.getBoardLength(); i++) {
			TableColumn column = new TableColumn(boardViewer, SWT.NONE);
			column.setText("Test");
		}
		for (int i = 0; i < count; i++) {
			TableItem item = new TableItem(boardViewer, SWT.NONE);
			item.setBackground(0, white);
			item.setBackground(1, gray);
			item.setBackground(2, white);
			item.setBackground(3, gray);
			item.setBackground(4, white);
			item.setBackground(5, gray);
			item.setBackground(6, white);
			item.setBackground(7, gray);
		}
		for (int i = 0; i < board.getBoardLength(); i++) {
			boardViewer.getColumn(i).pack();
		}
		boardViewer.setSize(boardViewer.computeSize(SWT.DEFAULT, 200));

		Group group = new Group(shlKandyDungeon, SWT.NONE);
		group.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		FormData fd_group = new FormData();
		fd_group.bottom = new FormAttachment(text, 0, SWT.BOTTOM);
		fd_group.top = new FormAttachment(grpGameplayButtons, 6);
		fd_group.right = new FormAttachment(list, 0, SWT.RIGHT);
		fd_group.left = new FormAttachment(label_1, 5);
		group.setLayoutData(fd_group);
		final int ii = playerCount;

		Button btnPlayerAdd = new Button(group, SWT.PUSH);
		btnPlayerAdd.addSelectionListener(new SelectionAdapter() {

			int iii = ii+1;

			@Override
			public void widgetSelected(SelectionEvent arg0) {

				board.setPlayers(newPlayerName.getText());
				board.getPlayerList().iterator().next().getName();
				listViewer.add(board.getPlayerList().get(iii).getName());
				System.out.println(newPlayerName.getText());
				iii++;
			}
		});
		btnPlayerAdd.setText("Add Player");

		newPlayerName = new Text(group, SWT.BORDER);

	}
}
