package framework;

/**
 * @author Michael Stickel
 * 
 */
public interface Event {

	/**
	 * @return
	 */
	public void pickEvent();
}
