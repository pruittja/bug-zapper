package framework;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Michael Stickel
 * 
 */
public class Player {

	protected Tile currentTile;
	protected String name;
	protected int row;
	protected int col;
	protected Map<String, Integer> properties;

	/**
	 * Constructor for Player class
	 * 
	 * @param name
	 * @param startTile
	 */
	public Player(String name, Tile startTile) {
		this.name = name;
		this.currentTile = startTile;
		properties = new HashMap<String, Integer>();
	}

	/**
	 * Constructor for Player class
	 * 
	 * @param name
	 */
	public Player(String name) {
		this.name = name;
		properties = new HashMap<String, Integer>();
	}

	/**
	 * Changes current position of player
	 * 
	 * @param nextTile
	 */
	public void changeTile(Tile nextTile) {
		this.currentTile = nextTile;
	}
	
	/**
	 * @param currentTile the currentTile to set
	 */
	public void setCurrentTile(Tile currentTile) {
		this.currentTile = currentTile;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}
	
	/**
	 * Sets the properties within the map.
	 * 
	 * @param key 
	 * @param value
	 */
	public void setProperties(String value, int key) {
		properties.put(value, key);
	}
	
	/**
	 * Gets player name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets current tile
	 * 
	 * @return currentTile
	 */
	public Tile getCurrentTile() {
		return currentTile;
	}

	/**
	 * Gets column
	 * 
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Gets column
	 * 
	 * @return col
	 */
	public int getCol() {
		return col;
	}
	
	/**
	 * Returns the entire properties map
	 * 
	 * @param key - location of value
	 * @return
	 */
	public Map<String, Integer> getProperties() {
		return properties;
	}
}
