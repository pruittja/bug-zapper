package framework;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

/**
 * @author Michael Stickel
 * 
 */
public class Board {

	protected int width;
	protected int height;
	protected int turn = 0;
	protected int rollNum;
	protected String filename;
	protected File file;
	protected Tile[][] board;
	protected ArrayList<Player> playerList;
	protected Player currentPlayer;
	protected Player winningPlayer;
	protected int winState = 0;
	protected Tile startingTile;
	protected Tile finishTile;
	
	
	/**
	 * Constructor for Board class. Populates board from txt file.
	 * 
	 * @param width
	 * @param height
	 * @throws Exception
	 */
	public Board(int width, int height, String startingFile) throws Exception {
		this.width = width;
		this.height = height;
		this.filename = startingFile;

		playerList = new ArrayList<Player>();
		
		board = new Tile[height][width];
		
		buildBoard();
	}
	
	public Board() {
		
	}
	
	public void buildBoard() {
		board = new Tile[height][width];
		file = new File(filename);	// Contains Board Path
		Scanner input = null;
		try {
			input = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int cellValue;

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				cellValue = input.nextInt();
				board[row][col] = new Tile(row, col, cellValue);
			}
		}
		input.close();
	}

	/**
	 * Displays board of tiles using checkIsPath() method to determine game
	 * path.
	 */
	public void displayBoard() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				System.out.print(board[row][col].checkIsPath());
				System.out.print(" ");
			}
			System.out.println("\n");
		}
	}

	/**
	 * Generates a random dice roll between 1 and 6
	 */
	public void roll() {
		Random rand = new Random();

		rollNum = rand.nextInt(6) + 1;
	}

	/**
	 * Ends current turn by incrementing turn field. Updates setTurn with new
	 * turn value.
	 */
	public void endTurn() {
		turn++;

		if (turn == playerList.size()) {
			turn = 0;
		}

		setTurn(turn);
	}

	/**
	 * Moves currentPlayer around board depending on number rolled
	 */
	public void movement() {
		int numLeft = rollNum;
		int count = 0;
		int row = currentPlayer.currentTile.getRow();
		int col = currentPlayer.currentTile.getCol();
		Tile nextTile = board[row][col];
		
		final int NORTH_ROW = row - 1, 
				  NORTH_COL = col;
		
		final int EAST_ROW = row, 
				  EAST_COL = col + 1;
		
		final int SOUTH_ROW = row + 1, 
				  SOUTH_COL = col;
		
		final int WEST_ROW = row, 
				  WEST_COL = col - 1;

		while (numLeft != 0) {
			numLeft--; // Decrements value of roll every loop

			switch (count++) {
			case 0: // north (path value = 3)
				if (isOnBoard(NORTH_ROW, NORTH_COL) == true
						&& board[NORTH_ROW][NORTH_COL].checkIsPath() == true
						&& board[NORTH_ROW][NORTH_COL].pathDirection().equals("North")) {
					nextTile = board[NORTH_ROW][NORTH_COL]; 
					count = 0;
					break;
				}
			case 1: // east (path value = 1)
				if (isOnBoard(EAST_ROW, EAST_COL) == true
						&& board[EAST_ROW][EAST_COL].checkIsPath() == true
						&& board[EAST_ROW][EAST_COL].pathDirection().equals("East")) {
					nextTile = board[EAST_ROW][EAST_COL]; 
					count = 0;
					break;
				}
			case 2: // south (path value = 2)
				if (isOnBoard(SOUTH_ROW, SOUTH_COL) == true
						&& board[SOUTH_ROW][SOUTH_COL].checkIsPath() == true
						&& board[SOUTH_ROW][SOUTH_COL].pathDirection().equals("South")) {
					nextTile = board[SOUTH_ROW][SOUTH_COL]; 
					count = 0;
					break;
				}
			case 3: // west (path value = 4)
				if (isOnBoard(WEST_ROW, WEST_COL) == true
						&& board[WEST_ROW][WEST_COL].checkIsPath() == true
						&& board[WEST_ROW][WEST_COL].pathDirection().equals("West")) {
					nextTile = board[WEST_ROW][WEST_COL]; 
					count = 0;
					break;
				}
			case 4: // finish (path value = 5)
				if (isOnBoard(WEST_ROW, WEST_COL) == true
						&& board[WEST_ROW][WEST_COL].checkIsPath() == true
						&& board[WEST_ROW][WEST_COL].pathDirection().equals("Finish")) {
					nextTile = board[WEST_ROW][WEST_COL];
					winningPlayer = currentPlayer; // if nextTile is "5", sets currentPlayer as winner
					count = 0;
					break;
				}
			default: count = 0;
			}
			row = nextTile.getRow(); // Updates row after move
			col = nextTile.getCol(); // updates col after move

		}
		if (isWinner(nextTile) == true) {  // Checks if next tile causes player to win
			winningPlayer = currentPlayer;
			winState = 1;				   
		}

		currentPlayer.changeTile(nextTile); // Moves player location
	}

	/**
	 * Determines whether next possible move is in bounds
	 * 
	 * @param row
	 *            - row number to check
	 * @param col
	 *            - col number to check
	 * 
	 * @return true - if next move is on board false - if next move is off board
	 */
	public boolean isOnBoard(int row, int col) {
		if (row >= 0 && col >= 0 && row < height && col < width)
			return true;
		else
			return false;
	}

	/**
	 * Checks to see if next move causes win
	 * 
	 * @param nextTile
	 *            - next possible tile to move onto
	 * 
	 * @return true - if next tile is winner false - if next tile is not winner
	 */
	public boolean isWinner(Tile nextTile) {
		if (nextTile.getRow() == finishTile.getRow() && nextTile.getCol() == finishTile.getCol())
			return true;
		else
			return false;
	}

	/**
	 * Adds players to Player List by name using set starting tile.
	 * 
	 * @param name
	 */
	public void setPlayers(String name) {
		playerList.add(new Player(name, startingTile));
	}
	
	
	/**
	 * Adds players to Player List by name starting tile.
	 * 
	 * @param name - name of Player
	 * @param startingTile - tile to set player on
	 */
	public void setPlayers(String name, Tile startingTile) {
		playerList.add(new Player(name, startingTile));
	}
	
	/**
	 * Replaces the current tile at a certain position with a new tile
	 * 
	 * @param row - row of tile to change
	 * @param col - column of tile to change
	 * @param tile - New Tile that will replace current
	 */
	public void setTile(int row, int col, Tile tile) {
		board[row][col] = tile;
	}
	
	public void setStartingTile(int row, int col) {
		this.startingTile = board[row][col];
	}
	
	public void setFinishTile(int row, int col) {
		this.finishTile = board[row][col];
	}
	
	/**
	 * Sets the currentPlayer depending on turn field
	 * 
	 * @param turn
	 */
	public void setTurn(int turn) {
		this.turn = turn;
		currentPlayer = playerList.get(turn);
	}

	/**
	 * Manually sets board width
	 * 
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	
	/**
	 * Manually sets filename
	 * 
	 * @param filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Manually sets currentPlayer
	 * 
	 * @param currentPlayer
	 */
	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	
	/**
	 * Manually sets board height
	 * 
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @param board the board to set
	 */
	public void setBoard(Tile[][] board) {
		this.board = board;
	}

	/**
	 * @param playerList the playerList to set
	 */
	public void setPlayerList(ArrayList<Player> playerList) {
		this.playerList = playerList;
	}

	/**
	 * @param winningPlayer the winningPlayer to set
	 */
	public void setWinningPlayer(Player winningPlayer) {
		this.winningPlayer = winningPlayer;
	}

	/**
	 * @param winState the winState to set
	 */
	public void setWinState(int winState) {
		this.winState = winState;
	}

	/**
	 * @param startingTile the startingTile to set
	 */
	public void setStartingTile(Tile startingTile) {
		this.startingTile = startingTile;
	}

	/**
	 * @param finishTile the finishTile to set
	 */
	public void setFinishTile(Tile finishTile) {
		this.finishTile = finishTile;
	}
	
	/**
	 * @param rollNum the rollNum to set
	 */
	public void setRollNum(int rollNum) {
		this.rollNum = rollNum;
	}
	
	/**
	 * @return currentPlayer
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * @return playerList
	 */
	public ArrayList<Player> getPlayerList() {
		return playerList;
	}

	/**
	 * @return length of board
	 */
	public int getLength() {
		return board.length;
	}

	/**
	 * @return return winningPlayer
	 */
	public Player getWinningPlayer() {
		return winningPlayer;
	}

	/**
	 * @return WinState
	 */
	public int getWinState() {
		return winState;
	}
	
	/**
	 * Returns the value of the rolled number
	 * 
	 * @return - rollNum
	 */
	public int getRollNum() {
		return rollNum;
	}

	/**
	 * Returns the current tile position.
	 * 
	 * @param row - current row
	 * @param col - current column
	 * @return
	 */
	public Tile getTile(int row, int col) {
		return board[row][col];
	}
	
	/**
	 * Getter method returns index of current player turn within playerlist
	 * 
	 * @return - turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * Gets board width
	 * 
	 * @return
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Gets board height
	 * 
	 * @return
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * gets filename
	 * 
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Returns startingTile object
	 * 
	 * @return
	 */
	public Tile getStartingTile() {
		return startingTile;
	}

	/**
	 * Returns finishTile object
	 * @return
	 */
	public Tile getFinishTile() {
		return finishTile;
	}
	
	/**
	 * Returns a 2D-array of Tile-objects
	 * @return The current 2D-array of Tile-objects.
	 */
	public Tile[][] getBoard(){
		return board;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}
}
