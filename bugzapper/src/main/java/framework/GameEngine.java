package framework;

/**
 * @author Michael Stickel
 * 
 */
public class GameEngine {

	protected int width; // Width of board
	protected int height; // Height of Board

	/**
	 * 
	 */
	public GameEngine() {

	}

	/**
	 * Alternate means of running program without GUI primarily for testing
	 * purposes
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception {

		setSize(8, 8);

		Board board = new Board(width, height, "board.txt");
		
		board.setStartingTile(0, 0);
		board.setFinishTile(7, 7);
		
		System.out.print(board.getStartingTile().getRow());
		System.out.println(board.getStartingTile().getCol());
		System.out.print(board.getFinishTile().getRow());
		System.out.println(board.getFinishTile().getCol());
		
		board.setFilename("board.txt");

		board.setPlayers("Michael");
		board.setTurn(0); // initial turn must be set.

		board.roll();
		board.movement();
	}

	/**
	 * Sets size of board externally. Must be set before creating board.
	 * 
	 * @param width
	 * @param height
	 */
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public static void main(String[] args) throws Exception {
		new GameEngine().run();

	}
}
