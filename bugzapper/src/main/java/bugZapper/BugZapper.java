package bugZapper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.Timer;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.wb.swt.SWTResourceManager;

import framework.Player;
import framework.Tile;

import bugZapper.boardElements.Bug;
import bugZapper.boardElements.Tower;
import bugZapper.boardElements.ZapperBoard;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.jface.action.Action;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.eclipse.swt.widgets.Combo;

public class BugZapper extends ApplicationWindow {

	private static ZapperBoard gameBoard;                 // The game board.
	
	private static MessageBox messageBox;				  // A message box that will
														  // be used for all messages.
	
	private static Timer attackTimer;					  // A timer that will tick when towers are attacking.
	private static Timer moveTimer;						  // A timer that will tick when bugs are moving. 
	
	private static ArrayList<Tower> towerList;			  // A list of all towers. 
	private static ArrayList<Label> towerLabelList;		  // A list of all labels associated with a tower. 
	private static HashMap<Tile, Label> pathLabelList;	  // A map of labels associated with the tiles on the board. 
	
	private static boolean placingTower;				  // A boolean that is toggled when the player is placing a tower. 
	private static boolean firstIteration;				  // A jerry-rigged variable for determining the starting tile.
	private static boolean closeWindow = true;
	
	private static Display display;						  // The current display for this SWT application. 

	private static Composite parent;
	private static Composite gameComp;
	private static Label statLabel;						  // The label that displays the player's money and lives. 
	private static Label bugLabel;						  // A label that stores the picture for bugs moving along the path. 
	
	private static Shell activeShell;
	
	private static FileSystemXmlApplicationContext context; // The Context from which to get all Spring beans. 
	
	private static final String SPLAT_IMAGE = "src\\main\\resources\\splat.jpg";
	private static final String OUCH_IMAGE ="src\\main\\resources\\ouch.jpg";
	private static final String BUG_IMAGE = "src\\main\\resources\\bug.jpg";
	private static final String BANG_IMAGE = "src\\main\\resources\\bang.jpg";
	private static final String TOWER_IMAGE = "src\\main\\resources\\tower.jpg";
	private static final String END_IMAGE = "src\\main\\resources\\computer.jpg";
	private static final String PATH_IMAGE = "src\\main\\resources\\lightning_bolt.jpg";
	private static final String BACKGROUND_IMAGE = "src\\main\\resources\\circuit_background.jpg";
	private static final String ERROR_IMAGE = "src\\main\\resources\\computer_error.jpg";
	
	private static final int NUM_ENEMIES = 400;			  // The number of enemies the player must kill in a round
	  													  // each time the round is reset. 
	private static final int NUM_LIVES = 40;			  // The number of lives the player gets when they reset. 
	private static final int MONEY_BALANCE = 5000;		  // The money they get when they reset. 
		
	private static int numEnemies = NUM_ENEMIES;		  // The amount of enemies the player has left to kill.
	private static int numLives = NUM_LIVES;			  // The number of lives the player has left. 
	private static int moneyBalance = MONEY_BALANCE;	  // The money the player has left. 

	
	/**
	 * Create the application window.
	 */
	public BugZapper() {
		super(null);
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		setBlockOnOpen(true);
		
	}
	
	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		
		this.parent = parent;
		
		activeShell = parent.getShell();
		activeShell.setMaximized(true);
		
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FormLayout());
		
		Composite hudComp = new Composite(container, SWT.NONE);
		
		FormData fd_hudComp = new FormData();
		fd_hudComp.left = new FormAttachment(0);
		fd_hudComp.right = new FormAttachment(100);
		fd_hudComp.bottom = new FormAttachment(100);
		hudComp.setLayoutData(fd_hudComp);
		
		gameComp = new Composite(container, SWT.NONE);
		
		FormData fd_gameComp = new FormData();
		fd_gameComp.top = new FormAttachment(0);
		fd_gameComp.left = new FormAttachment(0);
		fd_gameComp.bottom = new FormAttachment(0, 374);
		fd_gameComp.right = new FormAttachment(0, 454);
		gameComp.setLayoutData(fd_gameComp);
		
		GridLayout gl_gameComp = new GridLayout(gameBoard.getWidth(), true);
		gl_gameComp.horizontalSpacing = 0;
		gl_gameComp.verticalSpacing = 0;
		
		gameComp.setLayout(gl_gameComp);
		gameComp.setBackgroundImage(SWTResourceManager.getImage(BACKGROUND_IMAGE));
		
		fd_gameComp.bottom = new FormAttachment(hudComp, -6);
		fd_gameComp.right = new FormAttachment(hudComp, 0, SWT.RIGHT);
		
		makeStatLabel(hudComp);
		makeBuyButton(hudComp);
		makeUpgradeButton(hudComp);
		makeSellButton(hudComp);
		makeStartButton(hudComp);
		makeRestartButton(hudComp);
		makeLevelChooser(hudComp);
		
		buildBoard();

		return container;
	}
	
	private void makeBuyButton(Composite comp) {
		
		Button buyTowerButton = new Button(comp, SWT.NONE);
		buyTowerButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				if (moneyBalance >= 500) {
					
					placingTower = true;
					
				} else {
					
					messageBox = new MessageBox(activeShell, SWT.ICON_ERROR);
					messageBox.setText("Insufficient Funds");
					messageBox.setMessage("Not enough money. Kill more bugs!");
					messageBox.open();
					
				}
			}
		});
		
		buyTowerButton.setBounds(208, 29, 75, 25);
		buyTowerButton.setText("Buy Tower");
		
	}
	
	private void makeUpgradeButton(Composite comp) {
		
		Button upgradeButton = new Button(comp, SWT.NONE);
		upgradeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				attackTimer.stop();
				moveTimer.stop();
				
				if (towerList.size() > 0) {
					
					UpgradePrompt prompt = new UpgradePrompt(activeShell, moneyBalance, towerList);
					prompt.open();
					while (display.getFocusControl() == prompt.getShell()) {
						
					}
						
					moneyBalance = prompt.getCurrentMoney();
					updateLabel(null, null);
					
				} else {
					
					messageBox = new MessageBox(activeShell, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("No Towers");
					messageBox.setMessage("Towers must be purchased before ugprades can be applied.");
					messageBox.open();
					
				}
				
				if (gameBoard.getPlayerList().size() > 0) {
					attackTimer.start();
					moveTimer.start();
				}
			}
		});
		upgradeButton.setBounds(310, 29, 98, 25);
		upgradeButton.setText("Upgrade Towers");
	}
	
	private void makeSellButton(Composite comp) {
		
		Button sellButton = new Button(comp, SWT.NONE);
		sellButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				messageBox = new MessageBox(activeShell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				messageBox.setText("Confirm Sale");
				messageBox.setMessage("Would you like to sell ALL of your towers for: $" + towerList.size() * 1000 + "?");
				if (messageBox.open() == SWT.YES) {
					
					for (int i = 0; i < towerLabelList.size(); i++) {
						
						towerLabelList.get(i).setBackgroundImage(null);
						
					}
					
					moneyBalance += (towerList.size() * 1000);
					towerList = new ArrayList<Tower>();
					towerLabelList = new ArrayList<Label>();
					
				}
			}
		});
		sellButton.setBounds(447, 29, 75, 25);
		sellButton.setText("Sell Towers");
		
	}
	
	private void makeStartButton(Composite comp) {
		
		Button startStopButton = new Button(comp, SWT.NONE);
		startStopButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(attackTimer.isRunning()){

					moveTimer.stop();
					attackTimer.stop();
					
				}else{

					moveTimer.start();
					attackTimer.start();
					
				}
			}
		});
		startStopButton.setBounds(557, 29, 104, 25);
		startStopButton.setText("Start/Pause Round");
		
	}
	
	private void makeRestartButton(Composite comp) {
		
		Button restartButton = new Button(comp, SWT.NONE);
		restartButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				
				attackTimer.stop();
				moveTimer.stop();
				
				messageBox = new MessageBox(activeShell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				messageBox.setText("Restart?");
				messageBox.setMessage("Are you sure you want to restart the current round?");
				if (messageBox.open() == SWT.YES) {
					
					for (int i = 0; i < towerLabelList.size(); i++) {
						
						towerLabelList.get(i).dispose();
						
					}
					restart();
				}

			}
		});
		restartButton.setBounds(692, 29, 86, 25);
		restartButton.setText("Restart Round");
		
	}
	
	public static void restart() {
			
		for (HashMap.Entry<Tile, Label> entry : pathLabelList.entrySet()) {
			if (entry.getKey() != gameBoard.getFinishTile()) {
				updateLabel(entry.getValue(), PATH_IMAGE);
			} else {
				updateLabel(entry.getValue(), END_IMAGE);
			}
		}
			
		moneyBalance = MONEY_BALANCE;
		numEnemies = NUM_ENEMIES;
		numLives = NUM_LIVES;
			
		towerList = new ArrayList<Tower>();
		towerLabelList = new ArrayList<Label>();
		gameBoard.setPlayerList(new ArrayList<Player>());
			
		updateLabel(null, null);
			
		attackTimer.restart();
		attackTimer.stop();
		moveTimer.restart();
		moveTimer.stop();
	
	}
	
	private void makeStatLabel(Composite comp) {
		
		statLabel = new Label(comp, SWT.NONE);
		statLabel.setBounds(0, 0, 202, 64);
		FontData[] fontData = statLabel.getFont().getFontData();
		fontData[0].setHeight(16);
		statLabel.setFont(new Font(display, fontData[0]));
		updateLabel(null, null);
		
	}
	
	private void buildBoard() {
		
		// Set the width and height of each cell
		GridData gd= new GridData();
		
		gd.widthHint = display.getPrimaryMonitor().getBounds().width / gameBoard.getWidth();
		gd.heightHint = (display.getPrimaryMonitor().getBounds().height - 184) / gameBoard.getHeight();
		
		// This boolean determines if the starting
		// tile has been set yet. Currently, the framework
		// we were given does not set this for us. 
		firstIteration = true;
		
		// Loop through the board, and associate each
		// cell in the board's array with a label. 
		for(int i = 0; i < gameBoard.getLength(); i++){
			for(int j = 0; j< gameBoard.getHeight(); j++){
				
				// The label. 
				final Label label = new Label(gameComp, SWT.NONE);

				// Give the layout information about 
				// the size that it should be. 
				label.setLayoutData(gd);
				
				addInOutListeners(i, j, label);
				
				addClickListeners(i, j, label);
				
				addPathTiles(i, j, label);
			}
		}
	}
	
	private void addInOutListeners(final int row, final int column, final Label label) {
		
		// Add a listener to track when the mouse enters and
		// leaves this label. 
		label.addMouseTrackListener(new MouseTrackAdapter() {
			
			// When the mouse enters the label. 
			public void mouseEnter(MouseEvent e) {
				
				if (placingTower) {
					
					// If the player has their mouse hovering over a label
					// that is not associated with a tile along the path,
					// change the label's picture to a tower. 
					//
					// If the player is hovering over a path, we do not
					// want them to think they can set a tower on the path. 
					if (!gameBoard.getTile(row, column).checkIsPath()) {
						
						label.setBackgroundImage(SWTResourceManager.getImage(TOWER_IMAGE));
					}		
				}
			}
			
			// When the mouse leaves the label. 
			public void mouseExit(MouseEvent e) {
				
				// Check if the player is placing a tower
				// and that there is not already a tower on this label. 
				if (placingTower && !gameBoard.getTile(row, column).checkOccupied((ArrayList<Player>)((ArrayList<?>)towerList))) {
					
					// Check this label is not on the path. 
					if (!gameBoard.getTile(row, column).checkIsPath()) {
						
						// Clear the label's image. 
						label.setBackgroundImage(null);
						
					}
				}
			}					
		});
	}

	private void addClickListeners(final int row, final int column, final Label label) {
		// A listener for when the user clicks on this label. 
		label.addMouseListener(new MouseAdapter() {
			
			public void mouseUp(MouseEvent arg0) {
				
				if (placingTower) {
					System.out.println("Attempted place");
					// Make sure there is not already a tower on this tile
					// and that this tile is not along a path. 
					if (!gameBoard.getTile(row, column).checkIsPath() && 
							!gameBoard.getTile(row, column).checkOccupied((ArrayList<Player>)((ArrayList<?>)towerList))) { 
							
						// If the user clicked, it means they've
						// placed the tower, so deduct the cost of
						// the tower from their balance. 
						moneyBalance -= 1000;
						
						// Update the label to show the money spent. 
						updateLabel(null, null);
						
						placingTower = false;
						
						// Now, actually place a tower in this tile within the game board. 
						// Each tower is named Pew Pew, has a radius of 3 and a default damage of 150. 
						Tower tower = new Tower("Pew Pew", 3, 150, gameBoard, gameBoard.getTile(row, column));
						
						// Since the framework does not set the row 
						// and column of the player when the tile
						// is passed to the constructor, we must
						// manually set these. 
						tower.setRow(row);
						tower.setCol(column);
				
						// Add the tower to the list of towers
						// and add its associating label to the list
						// of labels.
						//
						// Now, the index of the item in one list
						// corresponds to its counterpart in the other. 
						towerList.add(tower);
						towerLabelList.add(label);
					
					// If the user tried to add the tower to an
					// occupied or path tile, tell them NO!
					} else {
						
						if (placingTower) {
							
							messageBox = new MessageBox(activeShell, SWT.ICON_ERROR | SWT.OK);
							messageBox.setText("Occupied!");
							messageBox.setMessage("You cannot place a tower here.");
							messageBox.open();
							
						}
					}
				}
			}
		});
	}
	
	private void addPathTiles(final int row, final int column, final Label label) {
		
		// Get the current tile in the loop. 
		Tile tile = gameBoard.getTile(row, column);
		
		// Check if it's on the path.  
		if(tile.checkIsPath()){
			
			// Since this is a tile along the path,
			// add it to the map of path tiles, along
			// with its corresponding label. 
			pathLabelList.put(tile, label);
			// Change the image to a lightning bolt. 
			updateLabel(label, PATH_IMAGE);

			if (firstIteration) {
				
				// Since the framework did not
				// set the starting tile, 
				// we must assume it's the first path
				// tile that is added to the board.
				gameBoard.setStartingTile(tile);
				firstIteration = false;
			}
			
			// Since the framework did not automatically set the
			// finishing tile either, we decided this would be
			// indicated in the text file with a 6. 
			if (tile.getCellValue() == 5) {
				
				gameBoard.setFinishTile(tile);
				
				// The bugs are trying to infect a computer, so set
				// the finishing tile's image to a picture of a computer. 
				//
				// This one's for you, Dr. Anvik ;). 
				updateLabel(label, END_IMAGE);
			}
		}
	}
	
	private void makeLevelChooser(Composite comp) {
		
		final Combo combo = new Combo(comp, SWT.READ_ONLY);
		combo.setBounds(1280, 10, 115, 23);
		combo.add("Choose Level:");
		combo.select(0);
		combo.add("Easy");
		combo.add("Harder");
		combo.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent arg0) {
				
				if (combo.getSelectionIndex() != combo.indexOf("Choose Level:")) {
					
					messageBox = new MessageBox(activeShell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
					messageBox.setText("Change Level?");
					messageBox.setMessage("Are you sure you want to change levels? All progress will be lost");
					if (messageBox.open() == SWT.YES) {
						
						if (combo.getSelectionIndex() == combo.indexOf("Easy")) {

							gameBoard = (ZapperBoard)context.getBean("easy_board");
							changeBoard();
							
						} else if (combo.getSelectionIndex() == combo.indexOf("Harder")) {

							gameBoard = (ZapperBoard)context.getBean("harder_board");
							changeBoard();
						}
					}	
				}
			}	
		});	
	}
	
	private void changeBoard() {
		
		Control[] children = parent.getChildren();
		for (int i = 0; i < children.length; i++) {
			children[i].dispose();
		}
		gameBoard.buildBoard();
		createContents(parent);
		restart();
		parent.layout();
	}
	
	private static void updateLabel(final Label label, final String filePath) {
		
		display.asyncExec(new Runnable() {
			public void run() {
				if (label != null) {
					if (!label.isDisposed()) {
						label.setBackgroundImage(SWTResourceManager.getImage(filePath));
					}
					
				}
				statLabel.setText(" Money: $" + moneyBalance + "\n  Lives: " + numLives);
			}
		});
		
	}
	
	private static class AttackEvent implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			if (!towerList.isEmpty()) {
					
				Iterator<Tower> towerItr = towerList.listIterator();
				Iterator<Label> labelItr = towerLabelList.listIterator();
				while (towerItr.hasNext()) {
					Tower tower = towerItr.next();
					final Label towerLabel = labelItr.next();
					Player target = tower.getCurrentTarget();
					
					if (target != null && tower.enemyReachable()) {
						
						bugLabel = null;
						
						for (HashMap.Entry<Tile, Label> entry : pathLabelList.entrySet()) {
							if (entry.getKey() == target.getCurrentTile()) {
								bugLabel = entry.getValue();
								break;
							}
						}
						
						if (tower.attackTarget()) {
							
							moneyBalance += 100;
							updateLabel(bugLabel, SPLAT_IMAGE);
							updateLabel(towerLabel, TOWER_IMAGE);
							
						} else {

							updateLabel(bugLabel, OUCH_IMAGE);
							updateLabel(towerLabel, BANG_IMAGE);

						}
														
					} else {

						updateLabel(towerLabel, TOWER_IMAGE);
						tower.findEnemy();

					}
				}
			}
		}
	}
	
	private static class MoveEvent implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if (numEnemies > 0) {

				gameBoard.getPlayerList().add(new Bug("Stack Overflow", gameBoard.getStartingTile(), 500));
				numEnemies--;
				
			} else if (gameBoard.getPlayerList().isEmpty()) {
				
				moveTimer.stop();
				attackTimer.stop();
				display.asyncExec(new Runnable() {
					public void run() {
						messageBox = new MessageBox(activeShell, SWT.ICON_WORKING);
						messageBox.setText("Winner!");
						messageBox.setMessage("All bugs have been decimated!");
						messageBox.open();
					}
				});
			}

			for(HashMap.Entry<Tile, Label> entry : pathLabelList.entrySet() ){
						
				if (entry.getKey() != gameBoard.getFinishTile()) {
					
					updateLabel(entry.getValue(), PATH_IMAGE);
					
				} else {
					
					updateLabel(entry.getValue(), END_IMAGE);
					
				}		
			}
					
			for (int i = 0; i < gameBoard.getPlayerList().size(); i++) {

				updateLabel(pathLabelList.get(gameBoard.getPlayerList().get(i).getCurrentTile()), BUG_IMAGE);

			}

			
			ArrayList<Player> enemyList = gameBoard.getPlayerList();
			
			for (int i = 0; i < enemyList.size(); i++) {
				
				gameBoard.setCurrentPlayer(enemyList.get(i));
				gameBoard.movement();

				
				if (gameBoard.getWinningPlayer() != null && gameBoard.getWinningPlayer() == enemyList.get(i)) {
					numLives--;
					
					updateLabel(pathLabelList.get(gameBoard.getFinishTile()), ERROR_IMAGE);
					
					gameBoard.setWinningPlayer(null);
					
					if (numLives < 1) {
						moveTimer.stop();
						attackTimer.stop();
						display.asyncExec(new Runnable() {
							public void run() {
								Control[] children = activeShell.getChildren();
								for (int i = 0; i < children.length; i++) {

									children[i].dispose();
								}
								Label blueScreenOfDeath = new Label (activeShell, SWT.NONE);
								blueScreenOfDeath.setBounds(0,0, display.getBounds().width, display.getBounds().height);
								blueScreenOfDeath.setBackground(display.getSystemColor(SWT.COLOR_DARK_BLUE));
								
								messageBox = new MessageBox(activeShell, SWT.ICON_ERROR | SWT.ABORT | SWT.RETRY | SWT.IGNORE);
								messageBox.setText("FATAL ERROR!");
								messageBox.setMessage("INTERNAL ERROR: code DEAD-00600");

								int response = messageBox.open();
								if (response == SWT.RETRY) {

									closeWindow = false;
									restart();
									activeShell.dispose();
									closeWindow = true;
									
								} else if (response == SWT.ABORT) {
									
									System.exit(0);
									
								} 
							}
						});
					}
					enemyList.remove(enemyList.get(i));
				}
			}	
		}
	}

	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		
		MenuManager helpMenu = new MenuManager("Help!");
		menuManager.add(helpMenu);
		
		Action helpAction = new Action("Help!") {
			public void run() {
				messageBox = new MessageBox(activeShell, SWT.ICON_QUESTION | SWT.OK);
				messageBox.setText("Help!");
				messageBox.setMessage(" Instructions:\n\n" + "To begin, select the 'Buy Tower' button at the bottom of the screen.\n" +
									  "This will enable the ability to place a tower anywhere on the board that  is not on a path and is not occupied by another tower.\n" +
									  " Each tower costs $1000 and the player starts with $" + MONEY_BALANCE + ".\n\n" +
									  " Upgrades may also be purchased for your towers; any upgrade  purchased is applied to ALL towers currently on the board.\n" +
									  " To purchase an upgrade, push the button at the bottom of the screen  and purchase the desired upgrade.\n\n" +
									  " Once you are satisfied with your tower arrangement, press the  Start/Pause Round button.\n" +
									  " Bugs will begin traversing the path, on their way to infecting a  computer at the other end.\n" +
									  " Each tower will shoot at a bug until either the bug is out of range, or  the bug is dead.\n" +
									  " Once a bug is dead, $100 will be earned for every tower that helped kill  the bug.\n\n" +
									  " The objective of the game is to eliminate all bugs before they infect the  computer.\n" +
									  " If a bug makes it to the computer, a life will be lost.\n" +
									  " If you allow more than " + NUM_LIVES + " bugs to infect the computer, you LOSE.\n\n" +
									  " At any time during a round, the game may be paused, and upgrades  and more towers may be purchased.\n" +
									  " However, it is not necessary to pause the game before making  purchases.");
				messageBox.open();
			}
		};
		
		helpAction.setAccelerator(SWT.F10);
		helpMenu.add(helpAction);
		
		return menuManager;
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Bug Zapper");
		newShell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		newShell.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				if (closeWindow) {
					System.exit(0);
				}
			}
		});
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(display.getPrimaryMonitor().getBounds().width, display.getPrimaryMonitor().getBounds().height);
	}
	

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {

		context = new FileSystemXmlApplicationContext("src\\main\\resources\\config.xml"); 
		gameBoard = (ZapperBoard)context.getBean("easy_board");
		gameBoard.buildBoard();

		attackTimer = new Timer(50, new AttackEvent());
		moveTimer = new Timer(100, new MoveEvent());
		
		towerList = new ArrayList<Tower>();
		towerLabelList = new ArrayList<Label>();
		pathLabelList = new HashMap<Tile, Label>();
		
		messageBox = new MessageBox(new Shell(), SWT.NONE);
		
		display = Display.getCurrent();

		while (true) {
				
			new BugZapper().open();

		}
	}
}
