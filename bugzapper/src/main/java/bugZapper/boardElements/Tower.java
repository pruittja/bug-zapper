package bugZapper.boardElements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import framework.Board;
import framework.Player;
import framework.Tile;

public class Tower extends Player {

	protected Player currentTarget;
	protected LinkedList<Tile> tileRadius;
	protected Board gameBoard;
	
	public Tower(String name, Tile startTile) {
		super(name, startTile);
		
	}

	public Tower(String name, int radius, int damage, Board board, Tile curTile) {
		super(name, curTile);
		gameBoard = board;
		setRadius(radius);
		setDamage(damage);
	}
	
	public void setRadius(int radius){
		properties.put("Radius", radius);	
		int row = -1;
		int column = -1;
		for (int i = 0; i < gameBoard.getWidth(); i++) {
			for (int j = 0; j < gameBoard.getHeight(); j++) {
				
				Tile tile = gameBoard.getBoard()[i][j];
				if (tile.getRow() == currentTile.getRow() && 
						tile.getCol() == currentTile.getCol()) {
					row = i;
					column = j;
				}
				
			}
		}
		
		tileRadius = new LinkedList<Tile>();
		
		for (int i = 0; i <= getRadius(); i++) {
			for (int j = 0; j <= getRadius(); j++) {
				
				try {
					tileRadius.add(gameBoard.getBoard()[row-i][column-j]);
				} catch (IndexOutOfBoundsException e) {
					
				}
				
				if (i != 0 && j != 0) {
					try {
						tileRadius.add(gameBoard.getBoard()[row+i][column+j]);
					} catch (IndexOutOfBoundsException e) {
						
					}
				}
				
				if (i != 0 || j != 0) {
					try {
						tileRadius.add(gameBoard.getBoard()[row-i][column+j]);
					} catch (IndexOutOfBoundsException e) {
						
					}
					try {
						tileRadius.add(gameBoard.getBoard()[row+i][column-j]);
					} catch (IndexOutOfBoundsException e) {
						
					}
				
				}
			}
			
		}
		
	}
	
	public int getRadius(){
		return properties.get("Radius");	
	}
	
	public void setCurrentTarget(Player enemy) {
		currentTarget = enemy;
	}
	
	public Player getCurrentTarget() {
		return currentTarget;
	}
	
	public void setDamage(int damage) {
		properties.put("Damage", damage);
	}
	
	public int getDamage() {
		return properties.get("Damage");
	}
	
	public boolean findEnemy(){

		Iterator<Player> itr = gameBoard.getPlayerList().iterator();
		while (itr.hasNext()) {
			
			Player enemy = itr.next();
			
			int health = enemy.getProperties().get("Health");
			if(health <= 0){
				gameBoard.getPlayerList().remove(enemy);
				continue;
			}
			
			for (int i = 0; i < tileRadius.size(); i ++) {
				if (enemy.getCurrentTile().getRow() == tileRadius.get(i).getRow() &&
								enemy.getCurrentTile().getCol() == tileRadius.get(i).getCol()) {
					
					currentTarget = enemy;
					
					return true;
				}
			}
		}
		
		return false;
		
	}
	
	public boolean enemyReachable() {
		
		if (currentTarget != null) {
			Iterator<Tile> itr = tileRadius.iterator();
			while (itr.hasNext()) {
				Tile nextTile = itr.next();
				if (currentTarget.getCurrentTile().getRow() == nextTile.getRow() && 
								currentTarget.getCurrentTile().getCol() == nextTile.getCol()) {
					
					return true;
				}
			}

			return false;
			
		} else {
			
			return findEnemy();
			
		}
	
	}
	
	public boolean attackTarget() {

		if (currentTarget != null) {
		
			currentTarget.getProperties().put("Health", currentTarget.getProperties().get("Health") - getDamage());
			
			if (currentTarget.getProperties().get("Health") <= 0 || currentTarget == null) {
			
				gameBoard.getPlayerList().remove(currentTarget);
				currentTarget = null;
				findEnemy();
				return true;
			
			} 
			
		}
		return false;
		
	}
	
	public void setGameBoard (Board board) {
		gameBoard = board;
	}
	

}
