package bugZapper.boardElements;

import framework.Player;
import framework.Tile;

public class Bug extends Player {

	public Bug(String name, Tile startTile, int initialHealth) {
		
		super(name, startTile);
		
		setHealth(initialHealth);
		
	}
	
	public Bug(String name) {
		
		super(name);
		
	}
	
	public void setHealth(int health) {
		
		properties.put("Health", health);
	}
	
	public int getHealth() {
		
		return properties.get("Health");
	}
	
	public boolean isDead() {
		
		if (properties.get("Health") <= 0) {
			return true;
		} else {
			return false;
		}
		
	}

}
