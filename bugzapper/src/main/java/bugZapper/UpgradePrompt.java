package bugZapper;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import bugZapper.boardElements.Tower;

public class UpgradePrompt extends Dialog {
	
	private Label liquidText;
	private Label memoryText;
	private int currentMoney;
	private static MessageBox box;
	private static ArrayList<Tower> currentTowers;

	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public UpgradePrompt(Shell parentShell, int money, ArrayList<Tower> towers) {
		super(parentShell);
		this.currentMoney = money;
		currentTowers = towers;
		
	}
	
	public int getCurrentMoney() {
		return currentMoney;
	}
	
	public boolean close() {
		this.getShell().dispose();
		return true;
	}
	
	private boolean buyUpgrade(int cost, String upgradeName) {
		
		box = new MessageBox(new Shell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		box.setMessage("Are you sure you want to buy " + upgradeName + " for $" + cost + "?");
		box.setText("Confirm Purchase");
		int userChoice = box.open();
		if (userChoice == SWT.YES) {
			
			if (currentMoney >= cost) {
				
				currentMoney -= cost;
				return true;
				
			} else {
				
				box = new MessageBox(new Shell(), SWT.ICON_ERROR);
				box.setText("Not Enough $$$$$");
				box.setMessage("Insufficient funds. Squash more bugs first!");
				box.open();
				
			}
			
		}
		
		return false;
		
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Composite liquidComp = new Composite(container, SWT.NONE);
		
		liquidText = new Label(liquidComp, SWT.BORDER);
		liquidText.setText("Damage + 100\nRadius+1\nCost: $2000");
		liquidText.setBounds(70, 177, 78, 50);
		
		Button liquidButton = new Button(liquidComp, SWT.NONE);
		liquidButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				// Somehow check if the tower has bought this upgrade before?
				
				if (buyUpgrade(2000, "Liquid Cooling")) {
					for (int i = 0; i < currentTowers.size(); i ++) {
						
						currentTowers.get(i).setDamage(currentTowers.get(i).getProperties().get("Damage") + 100);
						currentTowers.get(i).setRadius(currentTowers.get(i).getRadius() + 1);

					}
					
					close();

				}
				
			}
			
		});
		
		liquidButton.setBounds(70, 380, 75, 25);
		liquidButton.setText("Purchase");
		
		Label liquidCoolingLabel = new Label(liquidComp, SWT.NONE);
		liquidCoolingLabel.setLocation(0, 0);
		liquidCoolingLabel.setSize(239, 436);
		liquidCoolingLabel.setText("\n                Liquid Cooling");
		liquidCoolingLabel.setBackgroundImage(SWTResourceManager.getImage("src\\main\\resources\\water.jpg"));
		
		Composite memoryComp = new Composite(container, SWT.NONE);
		
		memoryText = new Label(memoryComp, SWT.BORDER);
		memoryText.setText("Damage + 50\n Cost: $500");
		memoryText.setBounds(80, 177, 75, 33);
		
		Button memoryButton = new Button(memoryComp, SWT.NONE);
		memoryButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if (buyUpgrade(500, "More Memory")) {
					for (int i = 0; i < currentTowers.size(); i ++) {
						currentTowers.get(i).setDamage(currentTowers.get(i).getProperties().get("Damage") + 50);
					}
					
					close();
				}
				
			}
			
		});
		
		memoryButton.setBounds(70, 380, 75, 25);
		memoryButton.setText("Purchase");
		
		Label addMemoryLabel = new Label(memoryComp, SWT.NONE);
		addMemoryLabel.setLocation(0, 0);
		addMemoryLabel.setSize(239, 436);
		addMemoryLabel.setText("\n                Add Memory");
		addMemoryLabel.setBackgroundImage(SWTResourceManager.getImage("src\\main\\resources\\RAM.jpg"));
	
		return container;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	 protected void configureShell(Shell shell) {
	      super.configureShell(shell);
	      shell.setText("Purchase Upgrade");
	   }

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(484, 515);
	}
}
