/**
 * 
 */
package framework.test;

import frameworkTEMP.Board;
import frameworkTEMP.Tile;
import junit.framework.TestCase;

/**
 * @author Michael Stickel
 *
 */
public class BoardTest extends TestCase {

	Board board;
	
	/**
	 * @param name
	 */
	public BoardTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		board = new Board(8, 8);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#Board(int, int)}.
	 * @throws Exception 
	 */
	public void testBoard() throws Exception {
		board = new Board(8, 8);		
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#displayBoard()}.
	 */
	public void testDisplayBoard() {
		board.displayBoard();
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#roll()}.
	 */
	public void testRoll() {	
		board.roll();
		
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
		assertTrue("Range between 1 and 6", board.getRoll() > 0 && board.getRoll() < 7);
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#getTurn()}.
	 */
	public void testGetTurn() {
		board.setPlayers("Michael");
		board.setPlayers("Branndon");
		board.setTurn(0);
		
		assertEquals(0, board.getTurn());
		
		board.endTurn();
		
		assertEquals(1, board.getTurn());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#setTurn(int)}.
	 */
	public void testSetTurn() {
		board.setPlayers("Michael");
		board.setPlayers("Branndon");
		board.setTurn(0);
		
		assertEquals("Michael", board.getCurrentPlayer().getName());
		
		board.setTurn(1);
		
		assertEquals("Branndon", board.getCurrentPlayer().getName());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#endTurn()}.
	 */
	public void testEndTurn() {
		board.setPlayers("Michael");
		board.setPlayers("Branndon");
		board.setTurn(0);
		
		assertEquals(0, board.getTurn());
		
		board.endTurn();
		
		assertEquals(1, board.getTurn());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#movement()}.
	 */
	public void testMovement() {
		board.setPlayers("Michael");
		board.setTurn(0);
		board.roll();
		
		assertEquals(0, board.getCurrentPlayer().getCurrentTile().getRow());
		assertEquals(0, board.getCurrentPlayer().getCurrentTile().getCol());
		
		board.movement();
		
		assertTrue("Shows movement from tile 0, 0", board.getCurrentPlayer().getCurrentTile().getCol() > 0);
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#isOnBoard(int, int)}.
	 */
	public void testIsOnBoard() {
		assertEquals(false, board.isOnBoard(-1,0));
	}
	
	/**
	 * Test method for {@link frameworkTEMP.Board#isWinner(Tile)}.
	 */
	public void isWinner() {
		Tile tile = new Tile(7, 7);
		
		assertEquals(true, board.isWinner(tile));
	}
	
	/**
	 * Test method for {@link frameworkTEMP.Board#getRoll()}.
	 */
	public void testGetRoll() {
		board.roll();
		
		assertTrue(board.getRoll() > 0 && board.getRoll() < 7);
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#getTile(int, int)}.
	 */
	public void testGetTile() {
		Tile tile = board.getTile(2, 3);
		
		assertEquals("Test row returned by getTile", 2, tile.getRow());
		assertEquals("Test col returned by getTile", 3, tile.getCol());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#setPlayers(java.lang.String)}.
	 */
	public void testSetPlayers() {
		board.setPlayers("Michael");
		board.setPlayers("Branndon");
		
		assertEquals("Michael", board.getPlayerList().get(0).getName());
		assertEquals("Branndon", board.getPlayerList().get(1).getName());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#getCurrentPlayer()}.
	 */
	public void testGetCurrentPlayer() {
		board.setPlayers("Michael");
		board.setTurn(0);
		
		assertEquals("Michael", board.getCurrentPlayer().getName());
	}

	/**
	 * Test method for {@link frameworkTEMP.Board#getPlayerList()}.
	 */
	public void testGetPlayerList() {
		board.setPlayers("Michael");
		board.setPlayers("Branndon");
		
		assertEquals("Michael", board.getPlayerList().get(0).getName());
		assertEquals("Branndon", board.getPlayerList().get(1).getName());
	}
}
