/**
 * 
 */
package framework.test;

import frameworkTEMP.Player;
import frameworkTEMP.Tile;
import junit.framework.TestCase;

/**
 * @author cs463001_37
 *
 */
public class PlayerTest extends TestCase {

	Player player;
	
	/**
	 * @param name
	 */
	public PlayerTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link frameworkTEMP.Player#Player(java.lang.String, frameworkTEMP.Tile)}.
	 */
	public void testPlayer() {
		Tile[][] board = new Tile[8][8];
		
		player = new Player("Michael", board[0][0]);
	}

	/**
	 * Test method for {@link frameworkTEMP.Player#getName()}.
	 */
	public void testGetName() {
		player = new Player("Michael");
		
		assertEquals("Michael", player.getName());
	}

	/**
	 * Test method for {@link frameworkTEMP.Player#changeTile(frameworkTEMP.Tile)}.
	 */
	public void testChangeTile() {
		Tile[][] board = new Tile[8][8];
		Tile tile = new Tile(0, 0, 1);
		
		board[0][0] = tile;
				
		player.changeTile(board[0][0]);
	}

	/**
	 * Test method for {@link frameworkTEMP.Player#getCurrentTile()}.
	 */
	public void testGetCurrentTile() {
		Tile[][] board = new Tile[8][8];
		Tile tile = new Tile(0, 0, 1);
		
				
		player.changeTile(tile);
		
		assertEquals(0, player.getCurrentTile().getRow());
	}
}
