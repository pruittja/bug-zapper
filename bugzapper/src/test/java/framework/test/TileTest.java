/**
 * 
 */
package framework.test;

import frameworkTEMP.Board;
import frameworkTEMP.Tile;
import junit.framework.TestCase;

/**
 * @author cs463001_37
 *
 */
public class TileTest extends TestCase {

	Tile tile;
	Board board;
	
	/**
	 * @param name
	 */
	public TileTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		tile = new Tile();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#Tile(int, int, int)}.
	 */
	public void testTileIntIntInt() {
		tile = new Tile(2, 3, 1);
		
		assertEquals(2,  tile.getRow());
		assertEquals(3,  tile.getCol());
		assertEquals(true,  tile.checkIsPath());

	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#Tile(int, int)}.
	 */
	public void testTileIntInt() {
		tile = new Tile(2, 3);
		
		assertEquals(2,  tile.getRow());
		assertEquals(3,  tile.getCol());
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#Tile()}.
	 */
	public void testTile() {
		tile = new Tile();
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#checkOccupied(java.util.ArrayList)}.
	 */
	public void testCheckOccupied() {
		try {
			board = new Board(8, 8);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Tile tile = new Tile(0, 0);
		board.setPlayers("Michael");
		board.getPlayerList();
		assertTrue(tile.checkOccupied(board.getPlayerList()) == true);
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#throwEvent(int, int)}.
	 */
	public void testThrowEvent() {
		tile.throwEvent(0, 3);
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#getRow()}.
	 */
	public void testGetRow() {
		tile = new Tile(2, 3);
		
		assertEquals(2, tile.getRow());
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#getCol()}.
	 */
	public void testGetCol() {
		tile = new Tile(2, 3);
		
		assertEquals(3, tile.getCol());
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#checkIsPath()}.
	 */
	public void testCheckIsPath() {
		tile = new Tile(2, 2, 1); // 1 = path
		
		assertEquals(true, tile.checkIsPath());
		
		tile = new Tile(2, 2, 0); // 0 = no path
		
		assertEquals(false, tile.checkIsPath());
	}

	/**
	 * Test method for {@link frameworkTEMP.Tile#pathDirection()}.
	 */
	public void testPathDirection() {
		tile = new Tile(2, 2, 1);
		
		assertEquals("East", tile.pathDirection());
		
		tile = new Tile(2, 2, 2);
		
		assertEquals("South", tile.pathDirection());
		
		tile = new Tile(2, 2, 3);
		
		assertEquals("North", tile.pathDirection());
		
		tile = new Tile(2, 2, 4);
		
		assertEquals("West", tile.pathDirection());
	}

}
